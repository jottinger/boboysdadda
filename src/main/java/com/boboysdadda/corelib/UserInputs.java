package com.boboysdadda.corelib;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Program: corelib
 * Filename: UserInputs.java
 * Author: James Dreier
 * Date: 19Mar2016
 * Description: A set of methods to accept various inputs from the user
 */
public class UserInputs {

    /**
     * Get a String input from a user
     *
     * @param message What string would you like to cue the user with
     * @return The string they enter
     */
    public  String getString(String message) {
        Scanner input = new Scanner(System.in);
        System.out.println(message);
        return input.nextLine();
    }

    /**
     * Get a string from user and verify that the password is valid.
     * currently checking for length between 8-15 characters.
     *
     * @param password The string you want verified
     * @return the boolean
     */
    public boolean passwordCheck(String password) {
        if (password.length() < 8 || password.length() > 15) {
            System.out.println("Password must be between 8 and 15 characters.");
            return false;
        }
        else System.out.println("Password is valid");
        return true;
    }

    /**
     * Create a String array of the options you want the user to have as a numbered menu.
     *
     * @param options The String array listing the options you want
     * @return an int value representing the option the user chose. 1 is the first option
     */
    public  int getIntOption(String[] options) {
        Scanner input = new Scanner(System.in);
        System.out.println("Please select one of the following options:");
        for(int i = 0; i < options.length; i++){
            System.out.println((i+1) + ": " +options[i]);
        }
        int userInput = 0;
        try {
            userInput = input.nextInt();
            input.nextLine();

        } catch (InputMismatchException e) {
            System.out.println("You did not give a valid input");
        }
        return userInput;
    }
}
