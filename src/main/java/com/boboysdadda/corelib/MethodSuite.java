package com.boboysdadda.corelib;

import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Program: Method Suite Tester
 * FileName: MethodSuite.java
 * Author: James Dreier
 * Date: 31 March 2016
 * Description: A set of methods for working with numbers to calculate sum, max, and average
 */
public class MethodSuite {
    /**
     * Get the max of two integers
     *
     * @param a first integer
     * @param b second integer
     * @return The larger of 2 integers
     */
    public int max(int a, int b){
        if (a > b) return a;
        else return b;
    }

    /**
     * Get the max of an int array
     *
     * @param numberArray an array of int
     * @return The largest number in the array
     */
    public int max(int[] numberArray){
        int b = 0;
        for (int a:numberArray) {
            if (b < a){
                b = a;
            }
        }
        return b;
    }

    /**
     * Get the average of a int array
     *
     * @param numberArray an array of int
     * @return the average of the array
     */
    public  int average(int[] numberArray){
        int result=0;
        int b = 0;
        for (int a : numberArray) {
            b += a;
            result = b/numberArray.length;
        }
        return result;
    }

    /**
     * Get the average of a double array
     *
     * @param numberArray an array of double
     * @return the average of the array
     */
    public double average(double[] numberArray){
        double result = 0;
        double b = 0;
        for (double a:numberArray){
            b+= a;
            result = b/numberArray.length;
        }
        return result;
    }

    /**
     * Get the sum of an double array
     *
     * @param numberArray an array of double
     * @return the sum of the array
     */
    public double total(double[] numberArray){
        double b =  0.0;
        for(double a: numberArray){
            b += a;
        }
        return b;
    }

    /**
     * Takes an array of numbers and adds only the even ones
     *
     * @param numberArray the int array you want to work with
     * @return the sum of the even numbers
     */
    public int addEvens(int[] numberArray){
        int a = 0;
        for(int b:numberArray){
            if (b%2 == 0){
                a += b;
            }
        }
        return a;
    }

    /**
     * Reverse array.
     *
     * @param stringArray the string array
     */
    public void reverseArray(String[] stringArray){
       Collections.reverse(Arrays.asList(stringArray));
        for(String a:stringArray){
            System.out.println(a);
        }

    }
}
