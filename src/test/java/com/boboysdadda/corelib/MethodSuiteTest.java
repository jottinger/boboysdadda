package com.boboysdadda.corelib;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class MethodSuiteTest {
    MethodSuite methods;


    @BeforeMethod
    public void setUp() throws Exception {
        methods = new MethodSuite();
    }

    @DataProvider
    public Object[][] maxSimpleInputs() {
        return new Object[][]{
                {1, 2, 2},
                {2, 1, 2},
                {1, -2, 1},
                {-2, 1, 1}
        };
    }

    @Test(dataProvider = "maxSimpleInputs")
    public void testMax(int a, int b, int m) {
        assertEquals(methods.max(a, b), m);
    }

    @DataProvider
    public Object[][] maxInputsArray() {
        return new Object[][]{
                {new int[]{1, 2, 3, 4}, 4},
                {new int[]{4}, 4},
                {new int[]{1, -2, -3, -4}, 1},
        };
    }

    @Test(dataProvider = "maxInputsArray")
    public void testMaxArray(int[] array, int m) {
        assertEquals(methods.max(array), m);
    }

    @DataProvider
    public Object[][] averageIntInputs() {
        return new Object[][]{
                {new int[]{1, 2, 3}, 2},
                {new int[]{0, 0, 0}, 0},
                {new int[]{1}, 1}
        };
    }

    @Test(dataProvider = "averageIntInputs")
    public void testAverageInts(int[] array, int a) {
        assertEquals(methods.average(array), a);
    }

    @DataProvider
    public Object[][] averageDoubleInputs() {
        return new Object[][]{
                {new double[]{1.0, 2.0, 3.0}, 2.0},
                {new double[]{0.0, 0.0, 0.0}, 0.0},
                {new double[]{1.0}, 1.0}
        };
    }

    @Test(dataProvider = "averageDoubleInputs")
    public void testAverageDoubles(double[] array, double m) {
        assertEquals(methods.average(array), m, 0.0001);
    }

    @Test
    public void testTotal() throws Exception {

    }

    @Test
    public void testAddEvens() throws Exception {

    }

    @Test
    public void testReverseArray() throws Exception {

    }

}